package demo.data;

import org.springframework.data.annotation.Id;

public class Device {

    @Id
    private String id;
    private LightStatus lightStatus;
    private LightSettings lightSettings;
    private Status status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LightStatus getLightStatus() {
        return lightStatus;
    }

    public void setLightStatus(LightStatus lightStatus) {
        this.lightStatus = lightStatus;
    }

    public LightSettings getLightSettings() {
        return lightSettings;
    }

    public void setLightSettings(LightSettings lightSettings) {
        this.lightSettings = lightSettings;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
