package demo.data;


import org.springframework.data.annotation.Id;

public class Status {
    private String time;
    private String uptime;
    private int rssi;
    private int fotoresistor;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUptime() {
        return uptime;
    }

    public void setUptime(String uptime) {
        this.uptime = uptime;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public int getFotoresistor() {
        return fotoresistor;
    }

    public void setFotoresistor(int fotoresistor) {
        this.fotoresistor = fotoresistor;
    }
}
