package demo.data;

import org.springframework.data.annotation.Id;

public class LightSettings {
    private int duration;
    private int brightness;
    private int dayPower;
    private int nightPower;
    private int nightStart;
    private int nightEnd;
    private int nightThreshold;
    private int nightIdlePower;
    private int nightLateHour;
    private int nightLatePower;
    private int speedOff;
    private int speedOn;
    private int fadeAmount;
    private String deviceName;


    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getBrightness() {
        return brightness;
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
    }

    public int getDayPower() {
        return dayPower;
    }

    public void setDayPower(int dayPower) {
        this.dayPower = dayPower;
    }

    public int getNightPower() {
        return nightPower;
    }

    public void setNightPower(int nightPower) {
        this.nightPower = nightPower;
    }

    public int getNightStart() {
        return nightStart;
    }

    public void setNightStart(int nightStart) {
        this.nightStart = nightStart;
    }

    public int getNightEnd() {
        return nightEnd;
    }

    public void setNightEnd(int nightEnd) {
        this.nightEnd = nightEnd;
    }

    public int getNightThreshold() {
        return nightThreshold;
    }

    public void setNightThreshold(int nightThreshold) {
        this.nightThreshold = nightThreshold;
    }

    public int getNightIdlePower() {
        return nightIdlePower;
    }

    public void setNightIdlePower(int nightIdlePower) {
        this.nightIdlePower = nightIdlePower;
    }

    public int getNightLateHour() {
        return nightLateHour;
    }

    public void setNightLateHour(int nightLateHour) {
        this.nightLateHour = nightLateHour;
    }

    public int getNightLatePower() {
        return nightLatePower;
    }

    public void setNightLatePower(int nightLatePower) {
        this.nightLatePower = nightLatePower;
    }

    public int getSpeedOff() {
        return speedOff;
    }

    public void setSpeedOff(int speedOff) {
        this.speedOff = speedOff;
    }

    public int getSpeedOn() {
        return speedOn;
    }

    public void setSpeedOn(int speedOn) {
        this.speedOn = speedOn;
    }

    public int getFadeAmount() {
        return fadeAmount;
    }

    public void setFadeAmount(int fadeAmount) {
        this.fadeAmount = fadeAmount;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
