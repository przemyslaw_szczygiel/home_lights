package demo.data;


import org.springframework.data.annotation.Id;

public class LightStatus {
    private int countDown;
    private int currentBrightness;
    private int startStat;
    private int getLastTriggerTime;

    public int getCountDown() {
        return countDown;
    }

    public void setCountDown(int countDown) {
        this.countDown = countDown;
    }

    public int getCurrentBrightness() {
        return currentBrightness;
    }

    public void setCurrentBrightness(int currentBrightness) {
        this.currentBrightness = currentBrightness;
    }

    public int getStartStat() {
        return startStat;
    }

    public void setStartStat(int startStat) {
        this.startStat = startStat;
    }

    public int getGetLastTriggerTime() {
        return getLastTriggerTime;
    }

    public void setGetLastTriggerTime(int getLastTriggerTime) {
        this.getLastTriggerTime = getLastTriggerTime;
    }
}
