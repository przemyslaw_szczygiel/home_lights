package demo.boundary;

import demo.control.DeviceRepository;
import demo.data.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@CrossOrigin
@RestController
public class DatabaseController {
   @Autowired
    private DeviceRepository deviceRepository;

    @PostMapping(path="/devices", consumes = "application/json")
    public void postDevice(@RequestBody Device device)
    {
        deviceRepository.save(device);
    }

    @PutMapping(path = "/devices")
    public void putDevices(@RequestBody Device device)
    {
        deviceRepository.save(device);
    }
    @RequestMapping(path = "/devices")
    public List<Device> getDevices() {
        Iterable<Device> devices = deviceRepository.findAll();
        List<Device> deviceList = new ArrayList<>();
        Iterator<Device> iterator = devices.iterator();
        while (iterator.hasNext()) {
            deviceList.add(iterator.next());
        }
        return deviceList;
    }

}
